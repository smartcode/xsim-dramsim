/* MC-draimsim.cpp: DRAMSim2 memory controller */
/*
 * __COPYRIGHT__ GT
 */

#include "../DRAMSim2/MemorySystem.h"
int SHOW_SIM_OUTPUT = FALSE;	// dramsim2 print output stuff..

#define COMPONENT_NAME "dramsim"

#ifdef MC_PARSE_ARGS
if(!strcasecmp(COMPONENT_NAME,type))
{
    int RQ_size;
    int FIFO;
    char dramsim_dev_str[64];
    char dramsim_sys_str[64];
    int megs_of_mem;

    if(sscanf(opt_string,"%*[^:]:%d:%d:%[^:]:%[^:]:%d", &RQ_size, &FIFO, dramsim_dev_str, dramsim_sys_str, &megs_of_mem) != 5)
        fatal("bad memory controller options string %s (should be \"draimsim:RQ-size:FIFO:drmasim-dev-ini:dramsim-sys-ini:megs-of-memory\")",opt_string);
    return new MC_dramsim_t(RQ_size, FIFO, dramsim_dev_str, dramsim_sys_str, megs_of_mem);
}
#else

/* This implements a very simple queue of memory requests.
   Requests are enqueued and dequeued in FIFO order, but the
   MC scans the requests to try to batch requests to the
   same page together to avoid frequent opening/closing of
   pages (or set the flag to force FIFO request scheduling). */
class MC_dramsim_t:public MC_t
{
protected:
    int RQ_size;        /* size of request queue, i.e. maximum number of outstanding requests */
    int RQ_num;         /* current number of requests */
    int RQ_head;        /* next transaction to send back to the cpu(s) */
    int RQ_req_head;    /* next transaction to send to DRAM */
    int RQ_tail;
    struct MC_action_t * RQ;

    bool FIFO_scheduling;
    md_paddr_t last_request;
    tick_t next_available_time;

    MemorySystem *dramsim[8];

public:
	static double dramsim_cum_bgpower[8];
	static double dramsim_cum_burstpower[8];
	static double dramsim_cum_refreshpower[8];
	static double dramsim_cum_actprepower[8];
	static unsigned long long dramsim_powercb_count[8];

	double dramsim_avg_bgpower;
	double dramsim_avg_burstpower;
	double dramsim_avg_refreshpower;
	double dramsim_avg_actprepower;
	double dramsim_avg_total_power;

    Callback_t *dramsim_read_cb;
	Callback_t *dramsim_write_cb;

	counter_t total_read_accesses;
	counter_t total_write_accesses;
	counter_t DWV_dev_access[8];

	tick_t total_read_latency;

	counter_t actual_unmodified_line_count;
	counter_t actual_unmodified_word_count;
	counter_t actual_unmodified_byte_count;

    void dramsim_read_complete(unsigned int id, unsigned long long address, unsigned long long clock_cycle)
    {
    	struct MC_action_t * req;
    	int idx = RQ_head;

		for(int i=0; i<RQ_num; i++) {
			req = &RQ[idx];

			if( (req->valid == true) && (req->cmd == CACHE_READ || req->cmd == CACHE_PREFETCH) && ((req->addr & ~63) == address) && (req->when_started <= sim_cycle) ) {
				req->dev_act &= ~((qword_t)(1 << id));

				if(!req->dev_act) {
					req->when_finished = sim_cycle;
					total_dram_cycles += req->when_finished - req->when_started;
				}

				break;
			}

			idx = modinc(idx, RQ_size);
		}
    }

    void dramsim_write_complete(unsigned int id, unsigned long long address, unsigned long long clock_cycle)
    {
    	struct MC_action_t * req;
    	int idx = RQ_head;

		for(int i=0; i<RQ_num; i++) {
			req = &RQ[idx];

			if( (req->valid == true) && (req->cmd == CACHE_WRITE || req->cmd == CACHE_WRITEBACK) && ((req->addr & ~63) == address) && (req->when_started <= sim_cycle) ) {
				req->dev_act &= ~((qword_t)(1 << id));

				if(!req->dev_act) {
					req->when_finished = sim_cycle;
					total_dram_cycles += req->when_finished - req->when_started;
				}

				break;
			}

			idx = modinc(idx, RQ_size);
		}
    }

    static void dramsim_power_cb(unsigned int id, double bgpower, double burstpower, double refreshpower, double actprepower)
    {
    	dramsim_cum_bgpower[id] += bgpower;
		dramsim_cum_burstpower[id] += burstpower;
		dramsim_cum_refreshpower[id] += refreshpower;
		dramsim_cum_actprepower[id] += actprepower;
		dramsim_powercb_count[id]++;
    }

public:

    MC_dramsim_t(const int arg_RQ_size, const int arg_FIFO, const char *arg_dramsim_dev_str, const char *arg_dramsim_sys_str, const int arg_megs_of_mem):
        RQ_num(0), RQ_head(0), RQ_req_head(0), RQ_tail(0), last_request(0), next_available_time(0) {
        init();

        if(skinflint == true)
			fprintf(stderr, "hello world!\n");
		else
			fprintf(stderr, "hey ehy\n");

        RQ_size = arg_RQ_size;
        FIFO_scheduling = arg_FIFO;
        RQ = (struct MC_action_t*) calloc(RQ_size,sizeof(*RQ));
        if(!RQ)
            fatal("failed to calloc memory controller request queue");

		// create dramsim instance
		dramsim_read_cb = new Callback<MC_dramsim_t, void, unsigned, uint64_t, uint64_t>(this, &MC_dramsim_t::dramsim_read_complete);
		dramsim_write_cb = new Callback<MC_dramsim_t, void, unsigned, uint64_t, uint64_t>(this, &MC_dramsim_t::dramsim_write_complete);

		for(int i=0; i<8; i++) {
			dramsim[i] = new MemorySystem(i, arg_dramsim_dev_str, arg_dramsim_sys_str, "/home/smartcode/workspace/SkinflintDevice/xsim-test/DRAMSim2", "", arg_megs_of_mem);
			if(!dramsim[i])
				fatal("failed to create dramsim instance");

			// connect callback functions
			dramsim[i]->RegisterCallbacks(dramsim_read_cb, dramsim_write_cb, &MC_dramsim_t::dramsim_power_cb);

			dramsim_cum_bgpower[i] = 0;
			dramsim_cum_burstpower[i] = 0;
			dramsim_cum_refreshpower[i] = 0;
			dramsim_cum_actprepower[i] = 0;
			dramsim_powercb_count[i] = 0;
		}

		dramsim_avg_bgpower = 0;
		dramsim_avg_burstpower = 0;
		dramsim_avg_refreshpower = 0;
		dramsim_avg_actprepower = 0;
		dramsim_avg_total_power = 0;
    }

    ~MC_dramsim_t() {
        free(RQ);
        RQ = NULL;

        // destroy dramsim instance
        for(int i=0; i<8; i++) {
        	delete dramsim[i];
			dramsim[i] = NULL;
        }

		delete dramsim_read_cb;
		dramsim_read_cb = NULL;
		delete dramsim_write_cb;
		dramsim_write_cb = NULL;
    }

    MC_ENQUEUABLE_HEADER {
        return RQ_num < RQ_size;
    }

    /* Enqueue a memory command (read/write) to the memory controller. */
    MC_ENQUEUE_HEADER {
        MC_assert(RQ_num < RQ_size,(void)0);
        struct MC_action_t * req = &RQ[RQ_tail];
        MC_assert(req->valid == false,(void)0);

        byte_t dev_act = 0xFF;

        if(cmd == CACHE_READ || cmd == CACHE_PREFETCH) {
			total_read_accesses++;
        }
		else if(cmd == CACHE_WRITE || cmd == CACHE_WRITEBACK) {
			total_write_accesses++;

			if(DWV_valid != true)
				fatal("DWV is invalid!");

			//fprintf(stderr, "DWV : 0x%llx\n", DWV);

			// unmodified line counting
			if(!DWV)
				actual_unmodified_line_count++;

			// unmodified word counting
			for(int i=0; i<8; i++) {
				if( !(DWV & ((qword_t)0xFF << (8 * i))) )
					actual_unmodified_word_count++;
			}

			// unmodified byte counting
			for(int i=0; i<64; i++) {
				if( !(DWV & ((qword_t)1 << i)) )
					actual_unmodified_byte_count++;
			}

			for(int i=7; i>=0; i--) {
        		if(!(DWV & (1 << i)) && !((DWV >> 8) & (1 << i)) && !((DWV >> 16) & (1 << i)) && !((DWV >> 24) & (1 << i)) &&
				   !((DWV >> 32) & (1 << i)) && !((DWV >> 40) & (1 << i)) && !((DWV >> 48) & (1 << i)) && !((DWV >> 56) & (1 << i))) {
					if(skinflint == true)
						dev_act &= ~((byte_t)1 << i);
				}
				else
					DWV_dev_access[i]++;
        	}

        	//fprintf(stderr, "dev act : 0x%02x\n", dev_act);

        	if(!dev_act) {
        		/* fill previous level as appropriate */
				if(prev_cp) {
					fill_arrived(prev_cp,MSHR_bank,MSHR_index);
					bus_use(uncore->fsb,1,req->cmd==CACHE_PREFETCH);
				}

        		return;
        	}
		}
		else
			fatal("unknown access!");

        req->valid = true;
        req->prev_cp = prev_cp;
        req->cmd = cmd;
        req->addr = addr;
        req->linesize = linesize;
        req->op = op;
        req->action_id = action_id;
        req->MSHR_bank = MSHR_bank;
        req->MSHR_index = MSHR_index;
        req->cb = cb;
        req->get_action_id = get_action_id;
        req->when_enqueued = sim_cycle;
        req->when_started = TICK_T_MAX;
        req->when_finished = TICK_T_MAX;
        req->when_returned = TICK_T_MAX;
        req->dev_act = dev_act;

        RQ_num++;
        RQ_tail = modinc(RQ_tail,RQ_size); //(RQ_tail + 1) % RQ_size;
        total_accesses++;
    }

    /* This is called each cycle to process the requests in the memory controller queue. */
    MC_STEP_HEADER {
        struct MC_action_t * req;

        /* MC runs at FSB speed */
        if(sim_cycle % uncore->fsb->ratio)
            return;

		// stepping dramsim instance
		for(int i=0; i<8; i++)
			dramsim[i]->update();

        if(RQ_num > 0) { /* are there any requests at all? */
            /* see if there's a new request to send to DRAM */
            if(next_available_time <= sim_cycle) {
                int req_index = -1;
                md_paddr_t req_page = (md_paddr_t)-1;

                /* try to find a request to the same page as we're currently accessing */
                int idx = RQ_head;
                for(int i=0; i<RQ_num; i++) {
                    req = &RQ[idx];
                    MC_assert(req->valid,(void)0);

                    if(req->when_started == TICK_T_MAX) {
                        if(req_index == -1) { /* don't have a request yet */
                            req_index = idx;
                            req_page = req->addr >> PAGE_SHIFT;
                            if(FIFO_scheduling || (req_page == last_request)) /* using FIFO, or this is access to same page as current access */
                                break;
                        }
                        if((req->addr >> PAGE_SHIFT) == last_request) { /* found an access to same page as current access */
                            req_index = idx;
                            req_page = req->addr >> PAGE_SHIFT;
                            break;
                        }
                    }
                    idx = modinc(idx,RQ_size);
                }

                if(req_index != -1) {
                    req = &RQ[req_index];
                    req->when_started = sim_cycle;
                    //req->when_finished = sim_cycle + dram->access(req->cmd, req->addr, req->linesize);
                    last_request = req_page;
                    next_available_time = sim_cycle + uncore->fsb->ratio;
                    //total_dram_cycles += req->when_finished - req->when_started;

                    // issue request to dramsim
                    // NOTE : address masking for dram transaction is hard-coded
                    if( (req->cmd == CACHE_READ) || (req->cmd == CACHE_PREFETCH) ) {
                    	for(int i=0; i<8; i++) {
                    		Transaction trans = Transaction(DATA_READ, (req->addr & ~63), NULL);
							dramsim[i]->addTransaction(trans);
                    	}
                    }
                    else if ( (req->cmd == CACHE_WRITE) || (req->cmd == CACHE_WRITEBACK) ) {
                    	for(int i=0; i<8; i++) {
                    		if(req->dev_act & ((byte_t)1 << i)) {
                    			Transaction trans = Transaction(DATA_WRITE, (req->addr & ~63), NULL);
								dramsim[i]->addTransaction(trans);
                    		}
                    	}
                    }
                }
            }

            /* walk request queue and process requests that have completed. */
            int idx = RQ_head;
            for(int i=0; i<RQ_num; i++) {
                req = &RQ[idx];

                if((req->when_finished <= sim_cycle) && (req->when_returned == TICK_T_MAX) && (!req->prev_cp || bus_free(uncore->fsb))) {
                    req->when_returned = sim_cycle;

                    total_service_cycles += sim_cycle - req->when_enqueued;

                    if(req->cmd == CACHE_READ || req->cmd == CACHE_PREFETCH)
						total_read_latency += req->when_finished - req->when_started;

                    /* fill previous level as appropriate */
                    if(req->prev_cp) {
                        fill_arrived(req->prev_cp,req->MSHR_bank,req->MSHR_index);
                        bus_use(uncore->fsb,req->linesize>>uncore->fsb_DDR,req->cmd==CACHE_PREFETCH);
                        break; /* might as well break, since only one request can writeback per cycle */
                    }
                }
                idx = modinc(idx,RQ_size);
            }

            /* attempt to "commit" the head */
            req = &RQ[RQ_head];
            if(req->valid) {
                if(req->when_returned <= sim_cycle) {
                    req->valid = false;
                    RQ_num--;
                    MC_assert(RQ_num >= 0,(void)0);
                    RQ_head = modinc(RQ_head,RQ_size); //(RQ_head + 1) % RQ_size;
                }
            }
        }
    }

    MC_PRINT_HEADER {
        fprintf(stderr,"<<<<< MC >>>>>\n");
        for(int i=0; i<RQ_size; i++) {
            fprintf(stderr,"MC[%d]: ",i);
            if(RQ[i].valid) {
                if(RQ[i].op)
                    fprintf(stderr,"%p(%lld)",RQ[i].op,((struct uop_t*)RQ[i].op)->decode.uop_seq);
                fprintf(stderr," --> %s",RQ[i].prev_cp->name);
                fprintf(stderr," MSHR[%d][%d]",RQ[i].MSHR_bank,RQ[i].MSHR_index);
            } else
                fprintf(stderr,"---");
            fprintf(stderr,"\n");
        }
    }

    MC_UNINIT_HEADER {
    	for(int i=0; i<8; i++) {
    		dramsim[i]->printStats();

    		dramsim_cum_bgpower[i] = dramsim_cum_bgpower[i] / dramsim_powercb_count[i] * 1000.0;
			dramsim_cum_burstpower[i] = dramsim_cum_burstpower[i] / dramsim_powercb_count[i] * 1000.0;
			dramsim_cum_refreshpower[i] = dramsim_cum_refreshpower[i] / dramsim_powercb_count[i] * 1000.0;
			dramsim_cum_actprepower[i] = dramsim_cum_actprepower[i] / dramsim_powercb_count[i] * 1000.0;

			dramsim_avg_bgpower += dramsim_cum_bgpower[i];
			dramsim_avg_burstpower += dramsim_cum_burstpower[i];
			dramsim_avg_refreshpower += dramsim_cum_refreshpower[i];
			dramsim_avg_actprepower += dramsim_cum_actprepower[i];
    	}

    	// calculate average power consumption (mW)
    	dramsim_avg_bgpower = dramsim_avg_bgpower / 8.0;
		dramsim_avg_burstpower = dramsim_avg_burstpower / 8.0;
		dramsim_avg_refreshpower = dramsim_avg_refreshpower / 8.0;
		dramsim_avg_actprepower = dramsim_avg_actprepower / 8.0;
		dramsim_avg_total_power = dramsim_avg_bgpower + dramsim_avg_burstpower + dramsim_avg_refreshpower + dramsim_avg_actprepower;
    }

    MC_REG_STATS_HEADER {
    	stat_reg_counter(sdb, true, "MC.total_accesses", "total accesses to memory controller",
						 &total_accesses, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, true, "MC.total_read_accesses", "total read accesses to memory controller",
						 &total_read_accesses, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, true, "MC.total_write_accesses", "total read accesses to memory controller",
						 &total_write_accesses, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, false, "MC.total_service_cycles", "cumulative request service cycles",
						 &total_service_cycles, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, false, "MC.total_read_latency", "cumulative read latency",
						 &total_read_latency, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_formula(sdb, true, "MC.service_average","average cycles per MC request",
						 "MC.total_service_cycles / MC.total_accesses",/* format */NULL);
		stat_reg_formula(sdb, true, "MC.average_read_latency","average latency of read request",
						 "MC.total_read_latency / MC.total_read_accesses",/* format */NULL);

		stat_reg_double(sdb, true, "dramsim.bg_power", "average DRAM background power consumption (mW)",
						 &dramsim_avg_bgpower, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_double(sdb, true, "dramsim.burst_power", "average DRAM burst power consumption (mW)",
						 &dramsim_avg_burstpower, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_double(sdb, true, "dramsim.refresh_power", "average DRAM refresh power consumption (mW)",
						 &dramsim_avg_refreshpower, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_double(sdb, true, "dramsim.actpre_power", "average DRAM act/pre power consumption (mW)",
						 &dramsim_avg_actprepower, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_double(sdb, true, "dramsim.total_power", "average DRAM total power consumption (mW)",
						 &dramsim_avg_total_power, /* initial value */0, TRUE, /* format */NULL);

		stat_reg_counter(sdb, false, "actual_unmodified_line_count", "actual unmodified line count",
						 &actual_unmodified_line_count, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, false, "actual_unmodified_word_count", "actual unmodified word count",
						 &actual_unmodified_word_count, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, false, "actual_unmodified_byte_count", "actual unmodified byte count",
						 &actual_unmodified_byte_count, /* initial value */0, TRUE, /* format */NULL);
		stat_reg_formula(sdb, true, "actual_unmodified_line","actual unmodified line portion",
						 "(actual_unmodified_line_count / MC.total_write_accesses) * 100",/* format */NULL);
		stat_reg_formula(sdb, true, "actual_unmodified_word","actual unmodified word portion",
						 "(actual_unmodified_word_count / (MC.total_write_accesses * 8)) * 100",/* format */NULL);
		stat_reg_formula(sdb, true, "actual_unmodified_byte","actual unmodified byte portion",
						 "(actual_unmodified_byte_count / (MC.total_write_accesses * 64)) * 100",/* format */NULL);

		stat_reg_counter(sdb, true, "DWV.dev1", "none",
						 &DWV_dev_access[0], /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, true, "DWV.dev2", "none",
						 &DWV_dev_access[1], /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, true, "DWV.dev3", "none",
						 &DWV_dev_access[2], /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, true, "DWV.dev4", "none",
						 &DWV_dev_access[3], /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, true, "DWV.dev5", "none",
						 &DWV_dev_access[4], /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, true, "DWV.dev6", "none",
						 &DWV_dev_access[5], /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, true, "DWV.dev7", "none",
						 &DWV_dev_access[6], /* initial value */0, TRUE, /* format */NULL);
		stat_reg_counter(sdb, true, "DWV.dev8", "none",
						 &DWV_dev_access[7], /* initial value */0, TRUE, /* format */NULL);
    }
};

double MC_dramsim_t::dramsim_cum_bgpower[8];
double MC_dramsim_t::dramsim_cum_burstpower[8];
double MC_dramsim_t::dramsim_cum_refreshpower[8];
double MC_dramsim_t::dramsim_cum_actprepower[8];
unsigned long long MC_dramsim_t::dramsim_powercb_count[8];


#endif /* MC_PARSE_ARGS */
#undef COMPONENT_NAME
